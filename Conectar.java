/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Docente;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class Conectar {
  private Connection con; 
 public Conectar(){
     obtenerConexion();
 }
 private void obtenerConexion(){
     try {
           con=(Connection)DriverManager.getConnection("jdbc:mysql://localhost/docente","root","");  
           JOptionPane.showMessageDialog(null, "Conexion establecida");
           
         
     } catch (SQLException e) {
         JOptionPane.showMessageDialog(null, "Error de conexion");
     }
 }
 public Statement crearSentencia(){
     try {
         return con.createStatement();
     } catch (SQLException e) {
         return null;
     }
 }
}
